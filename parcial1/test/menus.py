
class Menus:
    opcion = 0
    opcionc = 0
    opcionp = 0

    def menumedico(self):
        print("-----MÉDICO-----\n")
        print("- Presione 1 para registrar un médico\n"
              "- Presione 2 para modificar los datos del médico\n"
              "- Presione 3 para consultar los datos del médico\n"
              "- Presione 4 para eliminar el médico\n"
              "- Presione 5 para VOLVER\n")

        self.opcion = int(input("Ingrese un número: "))
        return self.opcion


    def menuclinica(self):
        print("-----CLÍNICA -----\n")
        print("- Presione 1 para registrar una clínica\n"
              "- Presione 2 para modificar los datos de la clínica\n"
              "- Presione 3 para consultar los datos de la clínica\n"
              "- Presione 4 para eliminar la clínica\n"
              "- Presione 5 para VOLVER\n")

        self.opcionc = int(input("Ingrese un número: "))
        return self.opcionc

    def menupaciente(self):
        print("-----PACIENTE-----\n")
        print("- Presione 1 para registrar un paciente\n"
              "- Presione 2 para modificar los datos del paciente\n"
              "- Presione 3 para consultar los datos del paciente\n"
              "- Presione 4 para eliminar el paciente\n"
              "- Presione 5 para VOLVER\n")

        self.opcionp = int(input("Ingrese un número: "))
        return self.opcionp







