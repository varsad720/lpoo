from menus import Menus
class Medico:
    nombre = ""
    edad = ""
    titulo = ""
    estado = False


    def main(self):
        while True:
            menus = Menus()
            menus.opcion = menus.menumedico()

            if menus.opcion == 1:
                print("------REGISTRO DE MÉDICO-----\n")

                self.nombre = input("Nombre: ")
                self.edad = input("Edad: ")
                self.titulo = input("Título: ")
                self.estado = True
                input("Medico registrado correctamente")

            if menus.opcion == 2:
                if self.estado == True:
                    print("------MODIFICAR DATOS DEL MÉDICO-----\n")

                    self.nombre = input("Nombre: ")
                    self.edad = input("Edad: ")
                    self.titulo = input("Titulo: ")
                else:
                    print("No se ha registrado un médico")


            if menus.opcion == 3:

                if self.estado == True:
                    print("----- Datos del médico", self.nombre, "-----\n")
                    print("Nombre:", self.nombre, "\nEdad:", self.edad, "\nTítulo:", self.titulo, "\n")

                else: print("No se ha registrado un médico")

            if menus.opcion ==4:

                self.estado=False
                self.nombre=""
                self.edad=""
                self.titulo=""
                print("REGISTRO DE MÉDICO ELIMINADO SATISFACTORIAMENTE.")
                input()

            elif menus.opcion == 5:
                print("SALIENDO")
                break
        return
