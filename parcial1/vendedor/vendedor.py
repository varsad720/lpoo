class Vendedor:
    nombre=""
    venta1= 0.0
    venta2=0.0
    venta3=0.0
    total=0.0
    comision=0.0

    def ventas(self):
        self.nombre=input("Ingrese su nombre:")
        print("------ Ventas mensuales de",self.nombre,"------")
        self.venta1=input("Venta #1:")
        self.venta2=input("Venta #2:")
        self.venta3=input("Venta #3:")

        self.total = float(self.venta1) + float(self.venta2) + float(self.venta3)

        self.comision = float(self.total) * 0.10

        print("-----Total-----\n\n","Total de ventas:",self.total,"\nComisión de",self.nombre,":",self.comision)
