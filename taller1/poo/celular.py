class Celular:
    marca=""
    capacidad=""
    camaras=""
    tipopantalla=""

    def indatos(self):
        print("Ingrese las especificaciones del dispositivo:")
        marca = input("Marca: ")
        capacidad = input("Almacenamiento: ")
        camaras = input("Número de cámaras: ")
        tipopantalla = input("Tipo de pantalla: ")
        self.marca = marca
        self.capacidad = capacidad
        self.camaras = camaras
        self.tipopantalla = tipopantalla

    def showspecs(self):
        print("Especificaciones del dispositivo: ")
        print("Celular marca",self.marca,"con",self.capacidad,"de almacenamiento, cuenta con",self.camaras,"camara(s) y pantalla con tecnología",self.tipopantalla)

cel1 = Celular()

cel1.indatos()
cel1.showspecs()

