class Pelicula:
    titulo=""
    genero=""
    año=""
    director=""

    def indata(self):
        print("Escriba los datos de la pelicula")
        titulo=input("Título de la película: ")
        genero=input("Género: ")
        año=input("Año de lanzamiento: ")
        director=input("Director:")
        self.titulo=titulo
        self.genero=genero
        self.año=año
        self.director=director

    def showdata(self):
        print("Datos de la pelicula: ")
        print("La obra de",self.genero,"del director",self.director,"fué lanzada en",self.año,"con el nombre de",self.titulo)

movie1=Pelicula()
movie1.indata()
movie1.showdata()
