class TransporteM:
    tipo=""
    tamaño=""
    motores=""

    def indata(self):
        print("Ingrese los datos del transporte")
        tipo=input("Tipo de vehiculo: ")
        tamaño=input("Tamaño del vehiculo: ")
        motores=input("Número de motores: ")
        self.tipo=tipo
        self.tamaño=tamaño
        self.motores=motores

    def showdata(self):
        print("Datos del vehículo: ")
        print(self.tipo,"de",self.motores," motores y un tamaño de",self.tamaño)

bote1=TransporteM()
bote1.indata()
bote1.showdata()