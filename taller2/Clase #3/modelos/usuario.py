from uuid import uuid3

class Usuario:
    """ Clase de Usuario para sistema de login"""
    # atributos
    id = 0
    nombre = ""
    apellido = ""
    correo = ""
    f_nacimiento = 2021
    estado = False
    rol = ""
    # Atributos extras
    nom_usuario = ""
    contrasenna = ""

    # Constructores
    def __init__(self):
        print("Esto es el metodo que inicializar los atributos de una clase")
        self.rol = "basico"
    # Metodos
    def registrar(self):
        print("\n=====> Registrar Usuario <======\n")

        print("I PARTE: Datos personales")
        self.id = uuid3
        self.nombre = input("Nombre: ")
        self.apellido = input("Apellido: ")
        print("Ingrese su correo: ")
        self.correo = input()
        self.f_nacimiento = int( input("Fecha de nacimiento: "))
        self.estado = True

        print("\nII PARTE: Datos del sistema")
        self.nom_usuario = input("Nombre de Usuario: ")
        self.contrasenna = input("Contraseña: ")

        print("\n USUARIO REGISTRADO \n")

    def reestablcer_pass(self):
        print("------------CAMBIAR CONTRASEÑA-----------")

        password =input("Contraseña actual:")
        if password == self.contrasenna:
            self.contrasenna=input("Nueva contraseña:")
        else:
            print("Contraseña incorrecta")





    def login(self,_user,_pass):
        """ Metodo para iniciar sesion de un usuario"""
        print("\n=====> Login de Usuario <======\n")

        # Validar usuario
        if self.id == 0:
            print("Aun no se han registrado el usuario en sistema!")
            return False

        # Usuario ya registrado
        if (self.nom_usuario == _user) and (self.contrasenna == _pass):
            print("Usuario correcto")
        elif (self.nom_usuario != _user) and (self.contrasenna == _pass):
            print("Nombre de usuario Incorrecto  >.<")
        elif  (self.nom_usuario == _user) and (self.contrasenna != _pass):
            print("Contraseña Incorrecta  >.<")
        else:
            print("Eroro en login")

    def consultar(self):
        print("--------Datos del usuario-------")
        print("Nombre de usuario:", self.nom_usuario,"\n"
              ,"Nombre:",self.nombre,"\n","Apellido:",self.apellido,"\n",
              "Correo:", self.correo,"\n","Fecha de nacimiento:",self.f_nacimiento,"\n","Contraseña:",self.contrasenna)


    def modificar(self):
        print("--------Modificar datos del usuario--------")
        self.nombre=input("Nombre: ")
        self.apellido=input("Apellido: ")
        self.correo=input("Correo: ")
        self.f_nacimiento=input("Fecha de nacimiento: ")

    def eliminar(self):
        self.id=0
        self.estado=False
        self.nombre=""
        self.apellido=""
        self.correo=""
        self.f_nacimiento=0
        self.contrasenna=""
        self.nom_usuario=""
print("Usuario eliminado con éxito.")
